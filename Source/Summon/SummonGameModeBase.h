// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "SummonGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class SUMMON_API ASummonGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
