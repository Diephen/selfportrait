// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Engine/DataTable.h"
#include "MyGameDataManager.generated.h"

// enum declare
UENUM(BlueprintType)
enum class ERecipeType : uint8 {
	None = 0        UMETA(DisplayName = "None"),
	Identical = 1   UMETA(DisplayName = "Identical"),
	Spell = 2       UMETA(DisplayName = "Spell"),
	Chaos = 3       UMETA(DisplayName = "Chaos"),
	Bug = 4         UMETA(DisplayName = "Bug"),
	Max = 5         UMETA(DisplayName = "Max")
};

UENUM(BlueprintType)
enum class ECustomCursorName : uint8 {
	None = 0        UMETA(DisplayName = "None"),
	Default = 1     UMETA(DisplayName = "Default"),
	Hand = 2        UMETA(DisplayName = "Hand"),
	Max = 3         UMETA(DisplayName = "Max")
};

UENUM(BlueprintType)
enum class ESourceType : uint8 {
	None = 0                 UMETA(DisplayName = "None"),
	SourceAir = 1            UMETA(DisplayName = "SourceAir"),
	SourceFear = 2           UMETA(DisplayName = "SourceFear"),
	SourceDirt = 3           UMETA(DisplayName = "SourceDirt"),
	SourceMeat = 4           UMETA(DisplayName = "SourceMeat"),
	SourceVoice = 5          UMETA(DisplayName = "SourceVoice"),
	SourceNonsense = 6       UMETA(DisplayName = "SourceNonsense"),
	Max = 7                  UMETA(DisplayName = "Max")
};

UENUM(BlueprintType)
enum class ESlotType : uint8 {
	Expriment = 0                 UMETA(DisplayName = "Expriment"),
	Tribute = 1                   UMETA(DisplayName = "Tribute"),
	Tool = 2                      UMETA(DisplayName = "Tool"),
	Worship = 3                   UMETA(DisplayName = "Worship"),
	Max = 5                       UMETA(DisplayName = "Max")
};





// TEST ON STRUCT 
USTRUCT(BlueprintType)
struct FItemData : public FTableRowBase

{

	GENERATED_USTRUCT_BODY()

public:
	FItemData()
		:Catagory("NONE")
		, IsRelic(0)
		, InventoryMax(0)
		, GameMax(0)
		, IsStackable(0)
		, CycleBeforeDecay(0)
		, CycleEndDrop("None")
		, IsInteractable(0)
		, MaxUsage(0)
		, ActionCompatability("NONE")
		, CommentHover("NONE")
	{}

	/** XP to get to the given level from the previous level */

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = ItemData)
		FString Catagory;

	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = ItemData)
		FString DisplayNameEn;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = ItemData)
		FString DisplayNameCh;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = ItemData)
		int32 ItemLevel;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = ItemData)
		int32 InventoryPiority;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = ItemData)
		int32 IsRelic;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = ItemData)
		int32 InventoryMax;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = ItemData)
		int32 GameMax;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = ItemData)
		int32 IsStackable;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = ItemData)
		int32 CycleBeforeDecay;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = ItemData)
		FString CycleEndDrop;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = ItemData)
		int32 IsInteractable;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = ItemData)
		int32 MaxUsage;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = ItemData)
		FString ActionCompatability;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = ItemData)
		FString ActionDrop;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = ItemData)
		FString CommentHover;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = ItemData)
		TAssetPtr<UTexture> Icon;

};


// recipe struct 
USTRUCT(BlueprintType)
struct FRecipeData : public FTableRowBase
{

	GENERATED_USTRUCT_BODY()

public:
	FRecipeData()
		:RecipeAuthor("P")
		, MaxDayTimeChange(1)
		, Comment("")
	{}

	/** XP to get to the given level from the previous level */

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = ItemData)
		FString RecipeAuthor;


	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = ItemData)
		FString Outcome;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = ItemData)
		int32 MaxDayTimeChange;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = ItemData)
		FString Comment;

};


// special recipe struct 
USTRUCT(BlueprintType)
struct FSpecialRecipeData : public FTableRowBase
{

	GENERATED_USTRUCT_BODY()

public:
	FSpecialRecipeData()
		:MaxDayTimeChange(1)
		, Comment("")
	{}

	/** XP to get to the given level from the previous level */

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = ItemData)
		FString Recipe;


	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = ItemData)
		FString Outcome;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = ItemData)
		int32 MaxDayTimeChange;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = ItemData)
		FString Comment;

};


// recipe struct 
USTRUCT(BlueprintType)
struct FDigMapData : public FTableRowBase
{

	GENERATED_USTRUCT_BODY()

public:
	FDigMapData()
	{}

	/** XP to get to the given level from the previous level */

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = MapData)
		int32 metal;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = MapData)
		int32 hair;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = MapData)
		int32 tooth;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = MapData)
		int32 skull;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = MapData)
		int32 lipstick;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = MapData)
		int32 worm;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = MapData)
		int32 mouse;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = MapData)
		int32 mirror;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = MapData)
		int32 watch;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = MapData)
		int32 total;

};






UCLASS()
class SUMMON_API AMyGameDataManager : public AActor
{
	GENERATED_BODY()

public:
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = ItemData)
	FDataTableRowHandle ItemDataTableHandle;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = ItemData)
	UDataTable *ItemDataTable;
	//UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = ItemData)
	FName RowName;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = ItemData)
	UDataTable* RecipeDataTable;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = ItemData)
	UDataTable* DigMapTable;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = ItemData)
	UDataTable* SpecialRecipeTable;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = ItemData)
	TArray<FString> AllDigDropNames;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = ItemData)
	TArray<FName> AllItemNames;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = ItemData)
	TArray<FName> AllSpecialRecipeIndex;


public:
	UFUNCTION(BlueprintCallable)
	FItemData GetItemData(FName ItemName, bool &Out_Exists);

	UFUNCTION(BlueprintCallable)
	FRecipeData GetRecipeData(FName InputItemName, bool &Out_Exists);

	UFUNCTION(BlueprintCallable)
	FSpecialRecipeData GetSpecialRecipeData(FName InputItemName, bool& Out_Exists);

	UFUNCTION(BlueprintCallable)
	FDigMapData GetDigMapData(FName ItemName, bool& Out_Exists);

	
public:	
	// Sets default values for this actor's properties
	AMyGameDataManager();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};

