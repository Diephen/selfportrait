// Fill out your copyright notice in the Description page of Project Settings.


#include "MyGameDataManager.h"

// Sets default values
AMyGameDataManager::AMyGameDataManager()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AMyGameDataManager::BeginPlay()
{
	Super::BeginPlay();

	RowName = "ItemName";
	ItemDataTableHandle.DataTable = ItemDataTable;
	ItemDataTableHandle.RowName = RowName;

	// get all names 
	AllItemNames = ItemDataTable->GetRowNames();
/*
#if WITH_EDITOR
	//Only include this line if building for editor
	AllDigDropNames = DigMapTable->GetColumnTitles();
#endif

*/
	
	AllSpecialRecipeIndex = SpecialRecipeTable->GetRowNames();

}

// Called every frame
void AMyGameDataManager::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

FItemData AMyGameDataManager::GetItemData(FName ItemName, bool &Out_Exists)
{
	FItemData* row = nullptr;
	FItemData result;
	if (ItemDataTable != nullptr && ItemDataTable)
	{
		
		row = ItemDataTable->FindRow<FItemData>(ItemName, "", true);
	}

	if (row)
	{
		result = *row;
		Out_Exists = true;
	}
	else
	{
		Out_Exists = false;
	}

	return result;

}

FRecipeData AMyGameDataManager::GetRecipeData(FName InputItemName, bool &Out_Exists)
{
	FRecipeData* row = nullptr;
	FRecipeData result;
	if (RecipeDataTable != nullptr && RecipeDataTable)
	{

		row = RecipeDataTable->FindRow<FRecipeData>(InputItemName, "", true);
	}

	if (row)
	{
		result = *row;
		Out_Exists = true;
	}
	else {
		Out_Exists = false;
	}

	return result;

}

FSpecialRecipeData AMyGameDataManager::GetSpecialRecipeData(FName InputItemName, bool& Out_Exists)
{
	FSpecialRecipeData* row = nullptr;
	FSpecialRecipeData result;
	if (SpecialRecipeTable != nullptr && SpecialRecipeTable)
	{

		row = SpecialRecipeTable->FindRow<FSpecialRecipeData>(InputItemName, "", true);
	}

	if (row)
	{
		result = *row;
		Out_Exists = true;
	}
	else {
		Out_Exists = false;
	}

	return result;

}

FDigMapData AMyGameDataManager::GetDigMapData(FName ItemName, bool& Out_Exists)
{
	FDigMapData* row = nullptr;
	FDigMapData result;
	if (DigMapTable != nullptr && DigMapTable)
	{

		row = DigMapTable->FindRow<FDigMapData>(ItemName, "", true);
	}

	if (row)
	{
		result = *row;
		Out_Exists = true;
	}
	else
	{
		Out_Exists = false;
	}

	return result;

}


