// Copyright Epic Games, Inc. All Rights Reserved.

#include "Summon.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, Summon, "Summon" );
